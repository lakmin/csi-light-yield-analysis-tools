# CsI light yield analysis tools
This repository has the analysis tools used for one photo electron measurement and light yield measurements for PMT's and MPPC's. 
The analysis tools are built in a user friendly way, where you can parse parameters through CL flags. So it requires the minimum editing of the scripts. 
The code is written in python, using pyROOT. So make sure **PyROOT** is correctly installed before running any of the scripts.


## Getting started
To be added ...

import ROOT as r
import numpy as np
import argparse
import sys
from array import array
# import ctypes

### Default ROOT Settings
r.TH1.AddDirectory(False)

def main():
    r.gROOT.SetBatch(True)
    r.gStyle.SetOptStat(0)

    args = GetParser()
    input_file = args.File
    hist_name = args.Hist
    PeakList = args.PeakList
    XMin = args.XMin
    XMax = args.XMax

    hist_file = r.TFile.Open(input_file)
    hist1 = hist_file.Get(hist_name)
    hist1.Rebin(5)

    # Run fit and catch the fit par's for the peaks
    FitParList, ParErrorList = RunOnePEfit(hist1, hist_name, PeakList, XMin, XMax)

    # Draw a linear graph with the peak ADC's
    DrawLinearGraph(FitParList, ParErrorList, hist_name, len(PeakList)*3)


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def GetParser():
    parser = argparse.ArgumentParser(
        description="This script is used to analyze the ADC output from 'Notice' fADC."
    )

    parser.add_argument(
        "-f",
        "--File",
        type=str,
        required=True,
        help="Path of the input root file"
    )

    parser.add_argument(
        "-n",
        "--Hist",
        type=str,
        required=True,
        help="Name of the histogram to fit"
    )

    parser.add_argument(
        "-l",
        "--PeakList",
        nargs='+',
        type=int,
        required=True,
        help="Give the list of peaks you wish to fit. It also supports negative peak. E.g: -l -50 0 75 150 225 300"
    )

    parser.add_argument(
        "-xmin",
        "--XMin",
        type=int,
        required=True,
        help="Give the minimum x value of the region you wish to fit."
    )

    parser.add_argument(
        "-xmax",
        "--XMax",
        type=int,
        required=True,
        help="Give the maximum x value of the region you wish to fit."
    )

    args = parser.parse_args()
    return args


def RunOnePEfit(hist, hist_name, PeakList, xmin_fit, xmax_fit):

    c1 = r.TCanvas("canv","",2600,2000)
    # PeakList = [0, 75, 145, 215, 285, 355, 425, 495, -50]
    PeakNameList1 = [] # List to store the peak names
    PeakNameList2 = [] # List to store the TLatex version of edited peak names
    NegPeakCounter = 0 # Counter for negative peaks (still not sure why we see -ve peaks)
    PhotoPeakCounter = 0 # Counter for photo electron peaks
    # Dynamically generate the peak names, even for pedestal and negative peaks
    for peak in PeakList:
        if peak < 0:
            NegPeakCounter += 1
            PeakNameList1.append("Neg. Peak " + str(NegPeakCounter))
        if peak == 0:
            PeakNameList1.append("Pedestal")
        if peak > 0:
            PhotoPeakCounter += 1
            PeakNameList1.append(str(PhotoPeakCounter) + " p.e. peak")

    GausList = [] # A list to store TF1 gaus distributions
    FitFuncStr = 'gaus(0)' # Define a string for the overall fit function

    for i, peak in enumerate(PeakList):
        GausList.append(r.TF1("Gaus"+str(i), 'gaus', peak-100,peak+100))
        if i != 0:
            FitFuncStr += '+gaus({})'.format(3*i)


    # Define the fit function
    FitFunc1 = r.TF1( 'FitFunc1', FitFuncStr, xmin_fit, xmax_fit )

    ############################
    ####### Run the fit ########
    ############################
    par = array( 'd', len(GausList)*3*[0.] )

    for i, Peak in enumerate(PeakList):
        if i == 0:
            hist.Fit( GausList[i], '0', '',  Peak-5, Peak+5)
        else:
            hist.Fit( GausList[i], '0+', '',  Peak-5, Peak+5)

    for i, Gaus in enumerate(GausList):
        par[3*i] = Gaus.GetParameters()[0]
        par[3*i+1] = Gaus.GetParameters()[1]
        par[3*i+2] = Gaus.GetParameters()[2]

    FitFunc1.SetParameters( par )

    FitFunc1.SetLineColorAlpha( r.kPink-9, 0.3 )
    FitFunc1.SetLineWidth( 6 )

    hist.SetLineWidth( 4 )
    hist.SetLineColor( r.kBlack )
    hist.SetMarkerStyle( 8 )
    hist.SetMarkerSize( 1.4 )

    hist.GetYaxis().SetTitle("NEvents")
    hist.GetXaxis().SetTitle("ADC")
    hist.SetTitle("MPPC one photon measurement")

    hist.Fit( FitFunc1, 'R+' )
    FitFunc1.Draw()
    hist.Draw("E0")

    ############################
    ######### Plotting #########
    ############################

    ymax = hist.GetMaximum()
    xmax = hist.GetXaxis().GetXmax()
    xmin = hist.GetXaxis().GetXmin()

    par_total = FitFunc1.GetParameters()
    par_errors = FitFunc1.GetParErrors()
    print('NDF: ', FitFunc1.GetNDF())
    print('Chi2: ', FitFunc1.GetChisquare())

    Fit_Chi2_NDF = r.TLatex(xmin+(xmax-xmin)*0.70, ymax*(1.0-0.05*len(GausList)-0.05), str(round(FitFunc1.GetChisquare(),3)) + " / " + str(FitFunc1.GetNDF()) + " = " + str(round(FitFunc1.GetChisquare()/FitFunc1.GetNDF(),3)))
    Fit_Chi2_NDF_Str = r.TLatex(xmin+(xmax-xmin)*0.60, ymax*(1.0-0.05*len(GausList)-0.05), "#chi^{2} / NDF: ")
    Fit_Chi2_NDF.SetTextFont(42)
    Fit_Chi2_NDF.SetTextSize(0.023)
    Fit_Chi2_NDF.SetTextColor(r.kAzure+4)
    Fit_Chi2_NDF_Str.SetTextFont(62)
    Fit_Chi2_NDF_Str.SetTextSize(0.023)
    Fit_Chi2_NDF_Str.Draw()
    Fit_Chi2_NDF.Draw()

    # Prepare the text box to write the fit results
    BkgBox = r.TBox(xmin+(xmax-xmin)*0.58, ymax*(1.0-0.05*len(GausList)), xmin+(xmax-xmin)*0.97, ymax*1.04)
    BkgBox.Draw()

    for i in range(len(GausList)):
        PeakNameStr = r.TLatex(xmin+(xmax-xmin)*0.60, ymax*(0.98-0.05*i), PeakNameList1[i]+": ")
        GausMean = r.TLatex(xmin+(xmax-xmin)*0.75, ymax*(0.98-0.05*i), str(round(par_total[3*i+1], 4)) + " #pm " + str(round(par_total[3*i+2], 4)))

        PeakNameStr.SetTextFont(62)
        GausMean.SetTextFont(42)
        PeakNameStr.SetTextSize(0.023)
        GausMean.SetTextSize(0.023)
        GausMean.SetTextColor(r.kAzure+4)

        TempList = [PeakNameStr, GausMean]
        PeakNameList2.append(TempList)

    for i, Gaus in enumerate(GausList):
        Gaus.SetParameters(par_total[3*i+0],par_total[3*i+1],par_total[3*i+2])
        # Gaus.SetLineColor( r.kAzure+4 )
        Gaus.SetLineColor( r.kAzure+4 )
        Gaus.SetLineWidth( -1504 )
        # Gaus.SetFillColorAlpha(r.kAzure+10, 0.2)
        Gaus.SetFillColorAlpha(r.kAzure+i+1, 0.5)
        Gaus.SetFillStyle( 3001 )
        Gaus.Draw('SAME')

        PeakNameList2[i][0].Draw()
        PeakNameList2[i][1].Draw()

    r.gPad.Modified()
    r.gPad.Update()
    c1.Draw()
    c1.SaveAs(hist_name+'.png')

    return par_total, par_errors


def DrawLinearGraph(parlist, par_errorlist, hist_name, ParLength):
    c1 = r.TCanvas("canv","",2600,2000)

    parlist.reshape((ParLength,))
    par_errorlist.reshape((ParLength,))

    PeakList = int(ParLength/3)
    ADC_vals, ADC_err, PE_vals = array('d'), array('d'), array('d')

    NegPeakCounter = 0

    for i in range(PeakList):
        if parlist[3*i+1] < 0:
            NegPeakCounter += 1
        else:
            PE_vals.append(i - NegPeakCounter)
            ADC_vals.append(parlist[3*i+1])
            ADC_err.append(par_errorlist[3*i+1])
    
    LinGraph = r.TGraphErrors(PeakList - NegPeakCounter, ADC_vals, PE_vals, ADC_err, r.nullptr)
    LinGraph.SetMarkerColor(r.kPink+4)
    LinGraph.SetMarkerStyle(8)
    LinGraph.SetMarkerSize(3)
    LinGraph.SetLineColor(r.kPink+9)
    LinGraph.SetLineWidth(7)
    LinGraph.Draw("AP")

    # The linear fit
    LinGraph.Fit("pol1", "F")
    FitGraph = LinGraph.GetFunction("pol1")
    FitGraph.SetLineColor(r.kGray+1)
    FitGraph.SetLineStyle(10)
    FitGraph.SetLineWidth(3)

    par_total = FitGraph.GetParameters()

    ############ Adding cosmetics to the graph ############
    LinGraph.GetYaxis().SetTitle("No. Photo Electrons")
    LinGraph.GetXaxis().SetTitle("ADC")
    LinGraph.SetTitle("MPPC one photon measurement - linear fit")

    PolFunction = r.TLatex(ADC_vals[1], len(PE_vals)-1.5, "y [p.e] = " + str(round(par_total[1], 3)) + "x [ADC] " + str(round(par_total[0], 3)))

    PolFunction.SetTextFont(42)
    # PeakNameStr.SetTextSize(0.023)
    PolFunction.SetTextSize(0.035)
    PolFunction.SetTextColor(r.kAzure+4)
    PolFunction.Draw()
    #######################################################

    c1.Draw()
    c1.SaveAs(hist_name+'_linear.png')

if __name__ == '__main__':
    sys.exit(main())
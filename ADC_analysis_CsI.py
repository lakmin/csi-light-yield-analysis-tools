import ROOT as r
import argparse
import sys, os
import numpy as np
from array import array

# import ctypes

### Default ROOT Settings
r.TH1.AddDirectory(False)

def main():
    args = GetParser()
    ntuple_file = args.File
    output_file = args.OutFile
    comment = args.Comment
    xmax = args.XMax
    splitADC = args.SplitADC

    ####### Define histogram ########
    canv = r.TCanvas("canv_ADC","",1500,1200)
    r.SetOwnership(canv, False)

    # Calculate the bins so that there is a good resolution
    if xmax < 1000:
        nbins = xmax + 100
    else:
        nbins = int((xmax + 100)/5)

    #ADC1, Pedestal1, PulseH1 represents channel 1. Similarly is for ADC2, Pedestal2 and PulseH2

    Pedestal1_hist = r.TH1F('Pedestal1_Hist',  'Pedestal1_Hist', 400, -50, 50)
    Pedestal2_hist = r.TH1F('Pedestal2_Hist',  'Pedestal2_Hist', 400, -50, 50)

    if splitADC:
        ADC1_hist1 = r.TH1F('ADC_Hist1_0_100', 'ADC_Hist1_0_100', 200, -100, 100)
        ADC1_hist2 = r.TH1F('ADC_Hist1_100_n', 'ADC_Hist1_100_n', nbins, 100, xmax)
        ADC2_hist1 = r.TH1F('ADC_Hist2_0_100', 'ADC_Hist2_0_100', 200, -100, 100)
        ADC2_hist2 = r.TH1F('ADC_Hist2_100_n', 'ADC_Hist2_100_n', nbins, 100, xmax)
    else:
        ADC1_hist = r.TH1F('ADC_Hist1', 'ADC_Hist1', nbins, -100, xmax)
        ADC2_hist = r.TH1F('ADC_Hist2', 'ADC_Hist2', nbins, -100, xmax)

    PulseH1_hist1 = r.TH1F('PulseH1_Hist',  'PulseH1_Hist', 1020, -20, 1000)
    PulseH2_hist1 = r.TH1F('PulseH2_Hist',  'PulseH2_Hist', 1020, -20, 1000)

    #### output_file ####
    if os.path.exists(output_file):
        outFile = r.TFile.Open(output_file, "UPDATE")
    else:
        outFile = r.TFile.Open(output_file, "RECREATE")

    #### read input file
    fadc_file = r.TFile.Open(ntuple_file)
    fadc_tree = fadc_file.Get("nfadc400")

    Reco_ADC = []
    #Decide which part to cut in the FADC
    baseline_cut_end = 100
    SR_cut_start = 150
    SR_cut_end = 250

    for i, entry in enumerate(fadc_tree):
        if i % 100 == 0:
            print("Done with " + bcolors.WARNING + str(i) + " events" + bcolors.ENDC)

        baseline1_selected = []
        baseline2_selected = []
        fadc_ch1_selected = []
        fadc_ch2_selected = []
        temp_weight_list = []

        fadc = getattr(entry, 'fadc/FADC')
        tag = getattr(entry, 'fadc/tag')
        trigger = getattr(entry, 'fadc/trigger')

        fadc.reshape((2048,))
        tag.reshape((6,))
        trigger.reshape((2,))
        
        # Channel 1~4 entries are shown in the list from 
        # 0~511, 512~1023, 1024~1535, 1536~2048 respectively.
        baseline_list1 = list(fadc)[0:baseline_cut_end]
        SR_list1 = list(fadc)[SR_cut_start:SR_cut_end]

        baseline_list2 = list(fadc)[511:511+baseline_cut_end]
        SR_list2 = list(fadc)[511+SR_cut_start:511+SR_cut_end]
        # tag_list = list(tag)
        # trigger_list = list(trigger)

        #Calculate the value for baseline
        baseline_avg1 = np.mean(baseline_list1)
        baseline_std1 = np.std(baseline_list1)
        baseline_avg2 = np.mean(baseline_list2)
        baseline_std2 = np.std(baseline_list2)

        # For pedestals, directly fill after minus the mean
        for i in baseline_list1:
            new_BL_val  = -1*(i - baseline_avg1)
            baseline1_selected.append(new_BL_val)
            temp_weight_list.append(1.0)

        for i in SR_list1:
            new_ADC_val = -1*(i - baseline_avg1)
            # Minus values should also be left in the list
            # so the sum will reduce the minus from the pedestal
            fadc_ch1_selected.append(round(new_ADC_val,3))

        for i in baseline_list2:
            new_BL_val  = -1*(i - baseline_avg2)
            baseline2_selected.append(new_BL_val)
            
        for i in SR_list2:
            new_ADC_val = -1*(i - baseline_avg2)
            fadc_ch2_selected.append(round(new_ADC_val,3))

        # intg_BL_ch1  = round(sum(basl_ch1_selected),4)
        # intg_BL_ch2  = round(sum(basl_ch2_selected),4)
        intg_ADC_ch1 = round(sum(fadc_ch1_selected),4)
        intg_ADC_ch2 = round(sum(fadc_ch2_selected),4)
        pulH_ADC_ch1 = round(max(fadc_ch1_selected),4)
        pulH_ADC_ch2 = round(max(fadc_ch2_selected),4)

        ######### Fill the histograms #########
        baseline1_array = array('d', baseline1_selected)
        baseline2_array = array('d', baseline2_selected)
        temp_weight_array = array('d', temp_weight_list)

        Pedestal1_hist.FillN(len(baseline1_selected), baseline1_array, temp_weight_array)
        Pedestal2_hist.FillN(len(baseline2_selected), baseline2_array, temp_weight_array)

        if splitADC:
            if intg_ADC_ch1 < 100:
                ADC1_hist1.Fill(intg_ADC_ch1)
            else:
                ADC1_hist2.Fill(intg_ADC_ch1)

            if intg_ADC_ch2 < 100:
                ADC2_hist1.Fill(intg_ADC_ch2)
            else:
                ADC2_hist2.Fill(intg_ADC_ch2)
        else:
            ADC1_hist.Fill(intg_ADC_ch1)
            ADC2_hist.Fill(intg_ADC_ch2)

        PulseH1_hist1.Fill(pulH_ADC_ch1)
        PulseH2_hist1.Fill(pulH_ADC_ch2)

    outFile.WriteObject(Pedestal1_hist, "Pedestal1_Hist_"+comment)
    outFile.WriteObject(Pedestal2_hist, "Pedestal2_Hist_"+comment)

    if splitADC:
        outFile.WriteObject(ADC1_hist1, "ADC_Ch1_hist_"+comment+'_0_100')
        outFile.WriteObject(ADC1_hist2, "ADC_Ch1_hist_"+comment+'_100_n')
        outFile.WriteObject(ADC2_hist1, "ADC_Ch2_hist_"+comment+'_0_100')
        outFile.WriteObject(ADC2_hist2, "ADC_Ch2_hist_"+comment+'_100_n')
    else:
        outFile.WriteObject(ADC1_hist, "ADC_Ch1_hist_"+comment)
        outFile.WriteObject(ADC2_hist, "ADC_Ch2_hist_"+comment)

    outFile.WriteObject(PulseH1_hist1, "PulH_Ch1_hist_"+comment)
    outFile.WriteObject(PulseH2_hist1, "PulH_Ch2_hist_"+comment)


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def GetParser():
    parser = argparse.ArgumentParser(
        description="This script is used to analyze the ADC output from 'Notice' fADC."
    )

    parser.add_argument(
        "-f",
        "--File",
        type=str,
        required=True,
        help="Path of the input root file"
    )

    parser.add_argument(
        "-o",
        "--OutFile",
        type=str,
        required=True,
        help="Path of the output root file"
    )

    parser.add_argument(
        "-c",
        "--Comment",
        type=str,
        required=True,
        help="Comment to the output file"
    )

    parser.add_argument(
        "-x",
        "--XMax",
        type=int,
        required=True,
        help="The maximum for the x-range"
    )

    parser.add_argument(
        "--SplitADC",
        action="store_true",
        help="Turn this flag if you wish to split the corrected ADC hist into 2, from 0-100, and 100~"
    )

    parser.add_argument(
        "-s",
        "--Sigma",
        type=int,
        required=False,
        help="The sigma cut to remove pedestal"
    )

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    sys.exit(main())


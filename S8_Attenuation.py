import ROOT as r
import numpy as np
import argparse
import sys
from array import array
# import ctypes

### Default ROOT Settings
r.TH1.AddDirectory(False)

def main():
    r.gROOT.SetBatch(True)
    r.gStyle.SetOptStat(0)

    args = GetParser()
    input_file = args.File
    hist_name = args.Hist
    hist_num = args.HistNum

    hist_list = []

    hist_file = r.TFile.Open(input_file)
    for i in range(1, 10):
        hist = hist_file.Get(hist_name+"_"+str(i))
        hist.Rebin(5)
        hist_list.append(hist)

    # Run fit and catch the fit par's for the peaks
    FitParList, ParErrorList = RunGausFit(hist_list, hist_name)

    # Draw a linear graph with the peak ADC's
    DrawAttenGraph(FitParList, ParErrorList, hist_name, hist_num)


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def GetParser():
    parser = argparse.ArgumentParser(
        description="This script is used to analyze the ADC output from 'Notice' fADC."
    )

    parser.add_argument(
        "-f",
        "--File",
        type=str,
        required=True,
        help="Path of the input root file"
    )

    parser.add_argument(
        "-n",
        "--Hist",
        type=str,
        required=True,
        help="Name of the histogram to fit"
    )

    parser.add_argument(
        "-i",
        "--HistNum",
        type=int,
        required=True,
        help="Number of histograms to plot iteratively for."
    )

    args = parser.parse_args()
    return args

def RunGausFit(hist_list, hist_name):

    c1 = r.TCanvas("canv","",2600,2000)
    PeakList = [14600, 8950, 5600, 3450, 2200, 1400, 850, 550, 14600]
    PeakNameList = []
    PeakName = ["0 dB", "4 dB", "8 dB", "12 dB", "16 dB", "20 dB", "24 dB", "28 dB", "Raw Sig"]

    Gaus1 = r.TF1("Gaus", 'gaus', PeakList[0]-200, PeakList[0]+200)
    Gaus2 = r.TF1("Gaus", 'gaus', PeakList[1]-200, PeakList[1]+200)
    Gaus3 = r.TF1("Gaus", 'gaus', PeakList[2]-200, PeakList[2]+200)
    Gaus4 = r.TF1("Gaus", 'gaus', PeakList[3]-100, PeakList[3]+100)
    Gaus5 = r.TF1("Gaus", 'gaus', PeakList[4]-100, PeakList[4]+100)
    Gaus6 = r.TF1("Gaus", 'gaus', PeakList[5]-100, PeakList[5]+100)
    Gaus7 = r.TF1("Gaus", 'gaus', PeakList[6]-100, PeakList[6]+100)
    Gaus8 = r.TF1("Gaus", 'gaus', PeakList[7]-100, PeakList[7]+100)
    Gaus9 = r.TF1("Gaus", 'gaus', PeakList[8]-200, PeakList[8]+200)

    GausList = [Gaus1, Gaus2, Gaus3, Gaus4, Gaus5, Gaus6, Gaus7, Gaus8, Gaus9]

    # # Save parameters for the linear fit
    ParList = []
    ParErrorList = []

    for i, Gaus in enumerate(GausList):
        print(bcolors.OKCYAN+"Running for histogram "+str(i)+bcolors.ENDC)
        hist_list[i].Fit( Gaus, '0+', '',  PeakList[i]-100,PeakList[i]+100)
        if i>4 and i<8:
            hist_list[i].Fit( Gaus, '0+', '',  PeakList[i]-50,PeakList[i]+50)

        ymax = hist_list[i].GetMaximum()
        xmax = PeakList[i] + 500
        xmin = PeakList[i] - 300

        hist_list[i].SetLineWidth( 4 )
        hist_list[i].SetLineColor( r.kBlack )
        hist_list[i].SetMarkerStyle( 8 )
        hist_list[i].SetMarkerSize( 1.4 )

        hist_list[i].GetYaxis().SetTitle("NEvents")
        hist_list[i].GetXaxis().SetTitle("ADC")
        hist_list[i].SetTitle("Signal attenuated for "+PeakName[i])

        hist_list[i].GetXaxis().SetRangeUser(xmin,xmax)
        hist_list[i].GetYaxis().SetRangeUser(0,ymax*1.1)
        hist_list[i].Draw("E0")

        ParList.append( Gaus.GetParameters()[1])
        ParErrorList.append( Gaus.GetParErrors()[1])

        if Gaus.GetNDF() > 0:
            Fit_Chi2_NDF = r.TLatex(xmin+800*0.69, ymax*(0.78), str(round(Gaus.GetChisquare(),3)) + " / " + str(Gaus.GetNDF()) + " = " + str(round(Gaus.GetChisquare()/Gaus.GetNDF(),3)))
        else:
            Fit_Chi2_NDF = r.TLatex(xmin+800*0.69, ymax*(0.78), "Weird fit?")

        Fit_Chi2_NDF_Str = r.TLatex(xmin+800*0.5, ymax*(0.78), "#chi^{2} / NDF: ")
        Fit_Chi2_NDF.SetTextFont(42)
        Fit_Chi2_NDF.SetTextSize(0.026)
        Fit_Chi2_NDF.SetTextColor(r.kAzure+4)
        Fit_Chi2_NDF_Str.SetTextFont(62)
        Fit_Chi2_NDF_Str.SetTextSize(0.026)
        Fit_Chi2_NDF_Str.Draw()
        Fit_Chi2_NDF.Draw()

        # Prepare the text box to write the fit results
        BkgBox = r.TBox(xmin+800*0.47, ymax*(0.82),xmin+800*0.92, ymax*0.9)
        BkgBox.Draw()

        PeakNameStr1 = r.TLatex(xmin+800*0.5, ymax*(0.85), "Gaus Peak: ")
        PeakMean1 = r.TLatex(xmin+800*0.69, ymax*(0.85), str(round(Gaus.GetParameters()[1], 4)) + " #pm " + str(round(Gaus.GetParameters()[2], 4)))
        PeakNameStr1.SetTextFont(62)
        PeakNameStr1.SetTextSize(0.026)
        PeakMean1.SetTextFont(42)
        PeakMean1.SetTextSize(0.026)
        PeakMean1.SetTextColor(r.kAzure+4)

        Gaus.SetParameters(Gaus.GetParameters()[0],Gaus.GetParameters()[1],Gaus.GetParameters()[2])
        Gaus.SetLineColor( r.kAzure+4 )
        Gaus.SetLineWidth( -1504 )
        Gaus.SetFillColorAlpha(r.kAzure+2, 0.5)
        Gaus.SetFillStyle( 3001 )
        Gaus.Draw('SAME')

        PeakNameStr1.Draw()
        PeakMean1.Draw()

        r.gPad.Modified()
        r.gPad.Update()
        c1.Draw()
        c1.SaveAs(hist_name+'_'+str(i+1)+'.png')

    # Create plot with the gaus dists.
    c2 = r.TCanvas("canv","",2600,2000)

    # ColorList = [r.kAzure+7, r.kTeal+5, r.kSpring-5, r.kOrange+1, r.kPink-5, r.kViolet+5, r.kGray+1]
    ColorList = r.kAzure+7

    # Some hacks
    GausList.reverse()
    GausList = GausList[:8]

    for j, Gaus in enumerate(GausList):
        Gaus.SetFillColorAlpha(r.kAzure+7, 0.5)
        Gaus.SetLineColor( r.kGray+2 )

        if j == 0:
            Gaus.SetRange(0,0,15000,4200)
            Gaus.GetYaxis().SetTitle("NEvents")
            Gaus.GetXaxis().SetTitle("ADC")
            Gaus.SetTitle("LV determination for 1 p.e measurement")
            Gaus.Draw()
        else:
            Gaus.Draw('SAME')

    c2.SaveAs(hist_name+'_all.png')

    return ParList, ParErrorList


def DrawAttenGraph(parlist, par_errorlist, hist_name, ParLength):
    c1 = r.TCanvas("canv","",2600,2000)

    dBList = array('d', [0, 4, 8, 12, 16, 20, 24, 28])
    ADC_vals = array('d', parlist[:8])
    ADC_err = array('d', par_errorlist[:8])
    
    LinGraph = r.TGraphErrors(ParLength, ADC_vals, dBList, ADC_err, r.nullptr)
    LinGraph.SetMarkerColor(r.kPink+4)
    LinGraph.SetMarkerStyle(8)
    LinGraph.SetMarkerSize(3)
    LinGraph.SetLineColor(r.kPink+9)
    LinGraph.SetLineWidth(7)
    LinGraph.Draw("AP")

    # Define a function for PMT gain
    f1 = r.TF1("f1", "[0]*TMath::Log10(x/[1])+[2]", 0, 15000)
    # f1.SetParLimits(0,0.02,0.05)
    f1.SetParLimits(0,-25,-15)
    f1.SetParLimits(1,parlist[8]-50,parlist[8]+50)

    # The linear fit
    LinGraph.Fit(f1, "R0")
    f1.SetLineColor(r.kGray+1)
    f1.SetLineStyle(10)
    f1.SetLineWidth(3)
    f1.Draw("SAME")

    par_total = f1.GetParameters()

    ############ Adding cosmetics to the graph ############
    LinGraph.GetYaxis().SetTitle("Decibels [dB]")
    LinGraph.GetXaxis().SetTitle("ADC")
    LinGraph.SetTitle("Atennuation Measurement - linear fit")

    PolFunction = r.TLatex(5000, 20, "y = " + str(round(par_total[0],3)) + " Log_{10}#left(#frac{x}{"+ str(round(par_total[1], 3)) + "}#right) " + str(round(par_total[2], 3)))

    PolFunction.SetTextFont(42)
    # PeakNameStr.SetTextSize(0.023)
    PolFunction.SetTextSize(0.04)
    PolFunction.SetTextColor(r.kAzure+4)
    PolFunction.Draw()
    #######################################################

    # Plot also the raw signal
    dBList2 = array('d', [0])
    ADC_vals2 = array('d', [parlist[8]])
    ADC_err2 = array('d', [par_errorlist[8]])
    LinGraph2 = r.TGraphErrors(1, ADC_vals2, dBList2, ADC_err2, r.nullptr)

    LinGraph2.SetMarkerColor(r.kAzure+8)
    LinGraph2.SetMarkerStyle(88)
    LinGraph2.SetMarkerSize(5)
    LinGraph2.SetLineColor(r.kOrange-2)
    LinGraph2.SetLineWidth(7)
    LinGraph2.Draw("P")

    c1.Draw()
    c1.SaveAs(hist_name+'_linear.png')

if __name__ == '__main__':
    sys.exit(main())
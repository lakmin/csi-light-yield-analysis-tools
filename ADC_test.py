import ROOT as r
import argparse
import sys, os
import numpy as np
from array import array

# import ctypes

### Default ROOT Settings
r.TH1.AddDirectory(False)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def GetParser():
    parser = argparse.ArgumentParser(
        description="This script is used to analyze the ADC output from 'Notice' fADC."
    )

    parser.add_argument(
        "-f",
        "--File",
        type=str,
        required=True,
        help="Path of the input root file"
    )

    args = parser.parse_args()
    return args

def main():
    args = GetParser()
    ntuple_file = args.File

    #### read input file
    fadc_file = r.TFile.Open(ntuple_file)
    fadc_tree = fadc_file.Get("nfadc400")

    for j, entry in enumerate(fadc_tree):

        fadc_selected = []
        baseline_corr = []

        fadc = getattr(entry, 'fadc/FADC')
        tag = getattr(entry, 'fadc/tag')
        trigger = getattr(entry, 'fadc/trigger')

        fadc.reshape((2048,))
        tag.reshape((6,))
        trigger.reshape((2,))

        baseline_list = list(fadc)[0:100]
        SR_list = list(fadc)[150:200]
        #Calculate the value for baseline
        baseline_avg = np.mean(baseline_list)
        baseline_std = np.std(baseline_list)

        for i in baseline_list:
            new_ADC_val = -1*(i - baseline_avg)
            baseline_corr.append(round(new_ADC_val,3))

        for i in SR_list:
            new_ADC_val = -1*(i - baseline_avg)
            fadc_selected.append(round(new_ADC_val,3))

        temp_val = round(sum(fadc_selected),4)

        #to avoid baseline only events
        if temp_val < 0:
            print("Baseline Avg: ", baseline_avg)
            print("Baseline Std: ", baseline_std)
            print("Integrated ADC: ", temp_val)
            print("Baseline ADC list: ", baseline_corr)
            print("Selected ADC list: ", fadc_selected)
            print('')

        if j == 50:
            break

if __name__ == '__main__':
    sys.exit(main())


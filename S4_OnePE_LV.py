import ROOT as r
import numpy as np
import argparse
import sys
from array import array
import math
# import ctypes

### Default ROOT Settings
r.TH1.AddDirectory(False)

def main():
    r.gROOT.SetBatch(True)
    r.gStyle.SetOptStat(0)

    args = GetParser()
    input_file = args.File
    hist_name = args.Hist

    hist_list = []

    hist_file = r.TFile.Open(input_file)
    for i in range(1, 8):
        hist = hist_file.Get(hist_name+"_"+str(i))
        hist.Rebin(5)
        hist_list.append(hist)

    # Run fit and catch the fit par's for the peaks
    RunOnePEfit(hist_list, hist_name)

    # Draw a linear graph with the peak ADC's
    # DrawGainGraph(FitParList, ParErrorList, hist_name, 7)


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def GetParser():
    parser = argparse.ArgumentParser(
        description="This script is used to analyze the ADC output from 'Notice' fADC."
    )

    parser.add_argument(
        "-f",
        "--File",
        type=str,
        required=True,
        help="Path of the input root file"
    )

    parser.add_argument(
        "-n",
        "--Hist",
        type=str,
        required=True,
        help="Name of the histogram to fit"
    )

    args = parser.parse_args()
    return args


def RunOnePEfit(hist_list, hist_name):

    c1 = r.TCanvas("canv","",2600,2000)
    PeakList = [-2.5, 50, 100, 150, 200]
    PeakNameList = []
    PeakName = ["1.44 V", "1.45 V", "1.46 V", "1.47 V", "1.48 V", "1.49 V", "1.50 V"]

    Gaus0 = r.TF1("Gaus0", 'gaus', PeakList[0]-50, PeakList[0]+50)

    Gaus11 = r.TF1("Gaus11", 'gaus', PeakList[1]-200, PeakList[1]+200)
    Gaus12 = r.TF1("Gaus12", 'gaus', PeakList[1]-200, PeakList[1]+200)
    Gaus13 = r.TF1("Gaus13", 'gaus', PeakList[1]-200, PeakList[1]+200)
    Gaus14 = r.TF1("Gaus14", 'gaus', PeakList[1]-200, PeakList[1]+200)
    Gaus15 = r.TF1("Gaus15", 'gaus', PeakList[1]-200, PeakList[1]+200)
    Gaus16 = r.TF1("Gaus16", 'gaus', PeakList[1]-200, PeakList[1]+200)
    Gaus17 = r.TF1("Gaus17", 'gaus', PeakList[1]-200, PeakList[1]+200)

    Gaus23 = r.TF1("Gaus23", 'gaus', PeakList[2]-200, PeakList[2]+200)
    Gaus24 = r.TF1("Gaus24", 'gaus', PeakList[2]-200, PeakList[2]+200)
    Gaus25 = r.TF1("Gaus25", 'gaus', PeakList[2]-200, PeakList[2]+200)
    Gaus26 = r.TF1("Gaus26", 'gaus', PeakList[2]-200, PeakList[2]+200)
    Gaus27 = r.TF1("Gaus27", 'gaus', PeakList[2]-200, PeakList[2]+200)

    Gaus35 = r.TF1("Gaus35", 'gaus', PeakList[3]-200, PeakList[3]+200)
    Gaus36 = r.TF1("Gaus36", 'gaus', PeakList[3]-200, PeakList[3]+200)
    Gaus37 = r.TF1("Gaus37", 'gaus', PeakList[3]-200, PeakList[3]+200)

    Gaus47 = r.TF1("Gaus47", 'gaus', PeakList[4]-200, PeakList[4]+200)

    GausList1 = [Gaus11, Gaus12, Gaus13, Gaus14, Gaus15, Gaus16, Gaus17]
    GausList2 = ['', '', Gaus23, Gaus24, Gaus25, Gaus26, Gaus27]
    GausList3 = ['', '', '', '', Gaus35, Gaus36, Gaus37]

    FitFunc1 = r.TF1( 'FitFunc1', 'gaus(0)+gaus(3)', -50, 350 )
    FitFunc2 = r.TF1( 'FitFunc2', 'gaus(0)+gaus(3)+gaus(6)', -50, 350 )
    FitFunc3 = r.TF1( 'FitFunc3', 'gaus(0)+gaus(3)+gaus(6)+gaus(9)', -50, 350 )
    FitFunc4 = r.TF1( 'FitFunc4', 'gaus(0)+gaus(3)+gaus(6)+gaus(9)+gaus(12)', -50, 350 )

    par1 = array( 'd', 2*3*[0.] )
    par2 = array( 'd', 3*3*[0.] )
    par3 = array( 'd', 4*3*[0.] )
    par4 = array( 'd', 5*3*[0.] )

    # # Save parameters for the linear fit
    # ParList = []
    # ParErrorList = []

    for i in range(7):
        print(bcolors.OKCYAN+"Running for histogram "+str(i)+bcolors.ENDC)
        hist_list[i].Fit( Gaus0, '0', '',  PeakList[0]-5,PeakList[0]+5)
        hist_list[i].Fit( GausList1[i], '0+', '',  PeakList[1]-5,PeakList[1]+5)
        par = par1
        FitFunc = FitFunc1
        if i>1:
            hist_list[i].Fit( GausList2[i], '0+', '',  PeakList[2]-5,PeakList[2]+5)
            par = par2
            FitFunc = FitFunc2
        if i>3:
            hist_list[i].Fit( GausList3[i], '0+', '',  PeakList[3]-5,PeakList[3]+5)
            par = par3
            FitFunc = FitFunc3
        if i>5:
            hist_list[i].Fit( Gaus47, '0+', '',  PeakList[4]-5,PeakList[4]+5)
            par = par4
            FitFunc = FitFunc4

        par[0] = Gaus0.GetParameters()[0]
        par[1] = Gaus0.GetParameters()[1]
        par[2] = Gaus0.GetParameters()[2]
        par[3] = GausList1[i].GetParameters()[0]
        par[4] = GausList1[i].GetParameters()[1]
        par[5] = GausList1[i].GetParameters()[2]

        if i>1:
            par[6] = GausList2[i].GetParameters()[0]
            par[7] = GausList2[i].GetParameters()[1]
            par[8] = GausList2[i].GetParameters()[2]
        if i>3:
            par[9] = GausList3[i].GetParameters()[0]
            par[10] = GausList3[i].GetParameters()[1]
            par[11] = GausList3[i].GetParameters()[2]
        if i>5:
            par[12] = Gaus47.GetParameters()[0]
            par[13] = Gaus47.GetParameters()[1]
            par[14] = Gaus47.GetParameters()[2]

        FitFunc.SetParameters( par )

        #######################################################

        FitFunc.SetLineColorAlpha( r.kPink-9, 0.3 )
        FitFunc.SetLineWidth( 6 )

        ymax = hist_list[i].GetMaximum()
        xmax = 350

        hist_list[i].SetLineWidth( 4 )
        hist_list[i].SetLineColor( r.kBlack )
        hist_list[i].SetMarkerStyle( 8 )
        hist_list[i].SetMarkerSize( 1.4 )

        hist_list[i].GetYaxis().SetTitle("NEvents")
        hist_list[i].GetXaxis().SetTitle("ADC")
        hist_list[i].SetTitle("LV determination for 1 p.e at "+PeakName[i])

        hist_list[i].Fit( FitFunc, 'R+' )
        FitFunc.Draw()
        hist_list[i].GetXaxis().SetRangeUser(-50,xmax)
        hist_list[i].Draw("E0")

        par_total = FitFunc.GetParameters()
        par_errors = FitFunc.GetParErrors()

        # ParList.append(par_total[4]-par_total[1])
        # ParErrorList.append(par_errors[4])

        y_offset = 0
        if i>1:
            y_offset = 0.05
        if i>3:
            y_offset = 0.1
        if i>5:
            y_offset = 0.15
            

        Fit_Chi2_NDF = r.TLatex(xmax*0.64, ymax*(0.78-y_offset), str(round(FitFunc.GetChisquare(),3)) + " / " + str(FitFunc.GetNDF()) + " = " + str(round(FitFunc.GetChisquare()/FitFunc.GetNDF(),3)))
        Fit_Chi2_NDF_Str = r.TLatex(xmax*0.45, ymax*(0.78-y_offset), "#chi^{2} / NDF: ")
        Fit_Chi2_NDF.SetTextFont(42)
        Fit_Chi2_NDF.SetTextSize(0.026)
        Fit_Chi2_NDF.SetTextColor(r.kAzure+4)
        Fit_Chi2_NDF_Str.SetTextFont(62)
        Fit_Chi2_NDF_Str.SetTextSize(0.026)
        Fit_Chi2_NDF_Str.Draw()
        Fit_Chi2_NDF.Draw()

        # Prepare the text box to write the fit results
        BkgBox = r.TBox(xmax*0.42, ymax*(0.82-y_offset), xmax*0.92, ymax*0.95)
        BkgBox.Draw()

        PedNameStr = r.TLatex(xmax*0.45, ymax*(0.9), " Pedestal: ")
        PeakNameStr1 = r.TLatex(xmax*0.45, ymax*(0.85), "1 p.e Peak: ")

        PedMean = r.TLatex(xmax*0.64, ymax*(0.9), str(round(par_total[1], 4)) + " #pm " + str(round(par_total[2], 4)))
        PeakMean1 = r.TLatex(xmax*0.64, ymax*(0.85), str(round(par_total[4], 4)) + " #pm " + str(round(par_total[5], 4)))

        PedNameStr.SetTextFont(62)
        PeakNameStr1.SetTextFont(62)
        PedNameStr.SetTextSize(0.026)
        PeakNameStr1.SetTextSize(0.026)

        PedMean.SetTextFont(42)
        PeakMean1.SetTextFont(42)
        PedMean.SetTextSize(0.026)
        PedMean.SetTextColor(r.kAzure+4)
        PeakMean1.SetTextSize(0.026)
        PeakMean1.SetTextColor(r.kAzure+4)

        if i>1:
            PeakNameStr2 = r.TLatex(xmax*0.45, ymax*(0.8), "2 p.e Peak: ")
            PeakNameStr2.SetTextFont(62)
            PeakNameStr2.SetTextSize(0.026)
            PeakMean2 = r.TLatex(xmax*0.64, ymax*(0.8), str(round(par_total[7], 4)) + " #pm " + str(round(par_total[8], 4)))
            PeakMean2.SetTextFont(42)
            PeakMean2.SetTextSize(0.026)
            PeakMean2.SetTextColor(r.kAzure+4)
        if i>3:
            PeakNameStr3 = r.TLatex(xmax*0.45, ymax*(0.75), "3 p.e Peak: ")
            PeakNameStr3.SetTextFont(62)
            PeakNameStr3.SetTextSize(0.026)
            PeakMean3 = r.TLatex(xmax*0.64, ymax*(0.75), str(round(par_total[10], 4)) + " #pm " + str(round(par_total[11], 4)))
            PeakMean3.SetTextFont(42)
            PeakMean3.SetTextSize(0.026)
            PeakMean3.SetTextColor(r.kAzure+4)
        if i>5:
            PeakNameStr4 = r.TLatex(xmax*0.45, ymax*(0.7), "4 p.e Peak: ")
            PeakNameStr4.SetTextFont(62)
            PeakNameStr4.SetTextSize(0.026)
            PeakMean4 = r.TLatex(xmax*0.64, ymax*(0.7), str(round(par_total[13], 4)) + " #pm " + str(round(par_total[14], 4)))
            PeakMean4.SetTextFont(42)
            PeakMean4.SetTextSize(0.026)
            PeakMean4.SetTextColor(r.kAzure+4)

        Gaus0.SetParameters(par_total[0],par_total[1],par_total[2])
        Gaus0.SetLineColor( r.kAzure+4 )
        Gaus0.SetLineWidth( -1504 )
        Gaus0.SetFillColorAlpha(r.kAzure+2, 0.5)
        Gaus0.SetFillStyle( 3001 )
        Gaus0.Draw('SAME')

        GausList1[i].SetParameters(par_total[3],par_total[4],par_total[5])
        GausList1[i].SetLineColor( r.kAzure+4 )
        GausList1[i].SetLineWidth( -1504 )
        GausList1[i].SetFillColorAlpha(r.kAzure+5, 0.5)
        GausList1[i].SetFillStyle( 3001 )
        GausList1[i].Draw('SAME')

        if i>1:
            GausList2[i].SetParameters(par_total[6],par_total[7],par_total[8])
            GausList2[i].SetLineColor( r.kAzure+4 )
            GausList2[i].SetLineWidth( -1504 )
            GausList2[i].SetFillColorAlpha(r.kAzure+7, 0.5)
            GausList2[i].SetFillStyle( 3001 )
            GausList2[i].Draw('SAME')
        if i>3:
            GausList3[i].SetParameters(par_total[9],par_total[10],par_total[11])
            GausList3[i].SetLineColor( r.kAzure+4 )
            GausList3[i].SetLineWidth( -1504 )
            GausList3[i].SetFillColorAlpha(r.kAzure+10, 0.5)
            GausList3[i].SetFillStyle( 3001 )
            GausList3[i].Draw('SAME')
        if i>5:
            Gaus47.SetParameters(par_total[12],par_total[13],par_total[14])
            Gaus47.SetLineColor( r.kAzure+4 )
            Gaus47.SetLineWidth( -1504 )
            Gaus47.SetFillColorAlpha(r.kTeal-1, 0.5)
            Gaus47.SetFillStyle( 3001 )
            Gaus47.Draw('SAME')

        PedNameStr.Draw()
        PedMean.Draw()
        PeakNameStr1.Draw()
        PeakMean1.Draw()
        if i>1:
            PeakNameStr2.Draw()
            PeakMean2.Draw()
        if i>3:
            PeakNameStr3.Draw()
            PeakMean3.Draw()
        if i>5:
            PeakNameStr4.Draw()
            PeakMean4.Draw()

        r.gPad.Modified()
        r.gPad.Update()
        c1.Draw()
        c1.SaveAs(hist_name+'_'+str(i+1)+'.png')

    # Create plot with the gaus dists.
    c2 = r.TCanvas("canv","",2600,2000)

    ColorList = [r.kAzure+7, r.kTeal+5, r.kSpring-5, r.kOrange+1, r.kPink-5, r.kViolet+5, r.kGray+1]

    # A hack to plot the graphs better (on top of each other)
    GausList1 = [Gaus16, Gaus15, Gaus14, Gaus13, Gaus12, Gaus11]
    GausList2 = [Gaus26, Gaus25, Gaus24, Gaus23]
    GausList3 = [Gaus36, Gaus35]

    for j in range(0,6):
        GausList1[j].SetFillColorAlpha(ColorList[j], 0.5)
        GausList1[j].SetLineColor( r.kGray+2 )

        if j == 0:
            GausList1[j].SetRange(-100,0,400,3000)
            GausList1[j].GetYaxis().SetTitle("NEvents")
            GausList1[j].GetXaxis().SetTitle("ADC")
            GausList1[j].SetTitle("LV determination for 1 p.e measurement")
            GausList1[j].Draw()
        else:
            GausList1[j].Draw('SAME')

        if j<4:
            GausList2[j].SetFillColorAlpha(ColorList[j], 0.5)
            GausList2[j].SetLineColor( r.kGray+1 )
            GausList2[j].Draw('SAME')
        if j<2:
            GausList3[j].SetFillColorAlpha(ColorList[j], 0.5)
            GausList3[j].SetLineColor( r.kGray+1 )
            GausList3[j].Draw('SAME')

        # c1.Draw()
        c2.SaveAs(hist_name+'_all.png')

    # return ParList, ParErrorList


def DrawGainGraph(parlist, par_errorlist, hist_name, ParLength):
    c1 = r.TCanvas("canv","",2600,2000)

    HVList = array('d', [2200, 2150, 2100, 2050, 1970, 1940, 1920])
    ADC_vals = array('d', parlist)
    ADC_err = array('d', par_errorlist)
    
    LinGraph = r.TGraphErrors(ParLength, HVList, ADC_vals, r.nullptr, ADC_err)
    LinGraph.SetMarkerColor(r.kPink+4)
    LinGraph.SetMarkerStyle(8)
    LinGraph.SetMarkerSize(3)
    LinGraph.SetLineColor(r.kPink+9)
    LinGraph.SetLineWidth(7)
    LinGraph.Draw("AP")

    # Define a function for PMT gain
    f_pg = r.TF1("f_pg", "[0]*TMath::Power(x,[1])+[2]", 1900, 2220)
    # f_pg.SetParLimits(0,0.02,0.05)
    f_pg.SetParLimits(1,4,10)

    # The linear fit
    LinGraph.Fit(f_pg, "R0")
    # FitGraph = LinGraph.GetFunction("f_pg")
    f_pg.SetLineColor(r.kGray+1)
    f_pg.SetLineStyle(10)
    f_pg.SetLineWidth(3)
    f_pg.Draw("SAME")

    par_total = f_pg.GetParameters()

    ############ Adding cosmetics to the graph ############
    LinGraph.GetYaxis().SetTitle("ADC")
    LinGraph.GetXaxis().SetTitle("HV [-V]")
    LinGraph.SetTitle("PMT Gain Measurement - linear fit")

    # A hack to get the significant digits
    SigDigits = round(math.log10(par_total[0]),0)-1

    PolFunction = r.TLatex(1930, 80, "y = " + str(round(par_total[0]/pow(10,SigDigits),3)) + "#times 10^{" + str(SigDigits) + "} x^{" + str(round(par_total[1], 3)) + "} " + str(round(par_total[2], 3)))

    PolFunction.SetTextFont(42)
    # PeakNameStr.SetTextSize(0.023)
    PolFunction.SetTextSize(0.04)
    PolFunction.SetTextColor(r.kAzure+4)
    PolFunction.Draw()
    #######################################################

    c1.Draw()
    c1.SaveAs(hist_name+'_linear.png')

if __name__ == '__main__':
    sys.exit(main())
import ROOT as r
import numpy as np
import argparse
import sys
import math

### Default ROOT Settings
r.TH1.AddDirectory(False)

def main():
    r.gROOT.SetBatch(True)
    r.gStyle.SetOptStat(0)

    args = GetParser()
    input_file = args.File
    hist_name = args.Hist
    peak = args.Peak
    xmax = args.XMax
    comment = args.Comment
    sigma = args.Sigma

    hist_file = r.TFile.Open(input_file)
    # Prepare two histograms for pedestal and the signal peak
    # they're named as 0_100 (for pedestal) and 100_n for signal peak
    hist1 = hist_file.Get(hist_name+"_0_100")
    hist2 = hist_file.Get(hist_name+"_100_n")

    hist1.Rebin(5)
    hist2.Rebin(20)

    if sigma == None:
        sigma_peak = 1500
    else:
        sigma_peak = sigma

    if args.Type == "Gaus":
        PedestalPar = MakePedestal(hist1, hist_name, 0, -50, 100, comment)
        RunGausOnBkgFit(hist2, hist_name, peak, 100, xmax, comment, PedestalPar, sigma_peak)

    elif args.Type == "Landau":
        PedestalPar = MakePedestal(hist1, hist_name, 0, -50, 100, comment)
        RunLandauOnBkgFit(hist2, hist_name, peak, 100, xmax, comment, PedestalPar, sigma_peak)

    elif args.Type == "Langaus":
        PedestalPar = MakePedestal(hist1, hist_name, 0, -50, 100, comment)
        RunLangauOnBkgFit(hist2, hist_name, peak, 100, xmax, comment, PedestalPar, sigma_peak)

    else:
        print("The type " + args.Type + " is not supported in this script. Please choose Gaus, Landau or Langaus.")
        exit


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def GetParser():
    parser = argparse.ArgumentParser(
        description="This script is used to analyze the ADC output from 'Notice' fADC."
    )

    parser.add_argument(
        "-f",
        "--File",
        type=str,
        required=True,
        help="Path of the input root file"
    )

    parser.add_argument(
        "-n",
        "--Hist",
        type=str,
        required=True,
        help="Name of the histogram to fit"
    )

    parser.add_argument(
        "-t",
        "--Type",
        type=str,
        required=True,
        help="The type of fit: 'Gaus', 'Landau' or 'Langaus':(Landau & Gaus Convolution) over Bkg."
    )

    parser.add_argument(
        "-p",
        "--Peak",
        type=int,
        required=True,
        help="The ADC number where roughly the peak is."
    )

    parser.add_argument(
        "-x",
        "--XMax",
        type=int,
        required=True,
        help="The ADC number for max of x"
    )

    parser.add_argument(
        "-c",
        "--Comment",
        type=str,
        required=True,
        help="Comment for hist title."
    )

    parser.add_argument(
        "-s",
        "--Sigma",
        type=int,
        required=False,
        help="The width of the signal."
    )

    args = parser.parse_args()
    return args


def MakePedestal(hist, hist_name, peak, xmin, xmax, comment):

    c1 = r.TCanvas("canv","",2600,2000)

    Gaus0 = r.TF1("Gaus0", 'gaus', peak-100, peak+100)
    FitFunc = r.TF1( 'FitFunc', '[0]*TMath::Gaus(x,[1],[2])', xmin, xmax )

    hist.Fit( Gaus0, '0', '',  -10, +10)
    FitFunc.SetParameter(0, Gaus0.GetParameters()[0])
    FitFunc.SetParameter(1, Gaus0.GetParameters()[1])
    FitFunc.SetParameter(2, Gaus0.GetParameters()[2])

    FitFunc.SetLineColorAlpha( r.kPink-9, 0.3 )
    FitFunc.SetLineWidth( 6 )

    hist.SetLineWidth( 4 )
    hist.SetLineColor( r.kBlack )
    hist.SetMarkerStyle( 8 )
    hist.SetMarkerSize( 1.4 )

    hist.GetYaxis().SetTitle("NEvents")
    hist.GetXaxis().SetTitle("ADC")
    hist.GetXaxis().SetRangeUser(-50,100)
    hist.SetTitle("Pedestal for " + comment)

    hist.Fit( FitFunc, 'R+' )
    FitFunc.Draw()
    hist.Draw("E0")

    ymax = hist.GetMaximum()

    par_total = FitFunc.GetParameters()
    par_errors = FitFunc.GetParErrors()
    print('NDF: ', FitFunc.GetNDF())
    print('Chi2: ', FitFunc.GetChisquare())

    Fit_Chi2_NDF = r.TLatex(60, ymax*0.75, str(round(FitFunc.GetChisquare(),3)) + " / " + str(FitFunc.GetNDF()) + " = " + str(round(FitFunc.GetChisquare()/FitFunc.GetNDF(),3)))
    Fit_Chi2_NDF_Str = r.TLatex(45, ymax*0.75, "#chi^{2} / NDF: ")
    Fit_Chi2_NDF.SetTextFont(42)
    Fit_Chi2_NDF.SetTextSize(0.023)
    Fit_Chi2_NDF.SetTextColor(r.kAzure+4)
    Fit_Chi2_NDF_Str.SetTextFont(62)
    Fit_Chi2_NDF_Str.SetTextSize(0.023)
    Fit_Chi2_NDF_Str.Draw()
    Fit_Chi2_NDF.Draw()

    # Prepare the text box to write the fit results
    BkgBox = r.TBox(40, ymax*0.82, 90, ymax*0.9)
    BkgBox.Draw()

    PedestalStr = r.TLatex(45, ymax*0.85, "Pedestal: ")
    PedestalStr.SetTextFont(62)
    PedestalStr.SetTextSize(0.023)

    PedMean = r.TLatex(60, ymax*0.85, str(round(par_total[1], 4)) + " #pm " + str(round(par_total[2], 4)))

    PedMean.SetTextFont(42)
    PedMean.SetTextSize(0.023)
    PedMean.SetTextColor(r.kAzure+4)

    PedestalStr.Draw()
    PedMean.Draw()

    Gaus0.SetParameters(par_total[0],par_total[1],par_total[2])
    Gaus0.SetLineColor( r.kAzure+4 )
    Gaus0.SetLineWidth( -1504 )
    Gaus0.SetFillColorAlpha(r.kAzure+2, 0.5)
    Gaus0.SetFillStyle( 3001 )
    Gaus0.Draw('SAME')

    r.gPad.Modified()
    r.gPad.Update()
    c1.Draw()
    c1.SaveAs(hist_name+'_pedestal.png')

    return par_total


def RunGausOnBkgFit(hist, hist_name, peak, xmin, xmax, comment, PedestalPar, sigma):

    c1 = r.TCanvas("canv","",2600,2000)

    Gaus1 = r.TF1("Gaus1", 'gaus', peak-sigma, peak+sigma)
    FitFunc = r.TF1( 'FitFunc', '[0]*TMath::Gaus(x,[1],[2])+[3]*TMath::Exp(x*[4])+[5]', xmin, xmax )

    hist.Fit( Gaus1, '0', '',  peak-3000, peak+3000)

    FitFunc.SetParameter(0, Gaus1.GetParameters()[0])
    FitFunc.SetParameter(1, Gaus1.GetParameters()[1])
    FitFunc.SetParameter(2, Gaus1.GetParameters()[2])
    FitFunc.SetParameter(3, 150 )
    FitFunc.SetParameter(4, -5*10e-4)
    FitFunc.SetParameter(5, -2)

    FitFunc.SetLineColorAlpha( r.kPink-9, 0.3 )
    FitFunc.SetLineWidth( 6 )

    hist.SetLineWidth( 4 )
    hist.SetLineColor( r.kBlack )
    hist.SetMarkerStyle( 8 )
    hist.SetMarkerSize( 1.4 )

    ymax = hist.GetMaximum()

    hist.GetYaxis().SetTitle("NEvents")
    # hist.GetYaxis().SetRangeUser(0,ymax)
    hist.GetXaxis().SetTitle("ADC")
    hist.GetXaxis().SetRangeUser(xmin,xmax)
    hist.SetTitle("Light yield measurement for " + comment)

    hist.Fit( FitFunc, 'R+' )
    FitFunc.Draw()
    hist.Draw("E0")

    par_total = FitFunc.GetParameters()
    # par_errors = FitFunc.GetParErrors()
    print('NDF: ', FitFunc.GetNDF())
    print('Chi2: ', FitFunc.GetChisquare())

    Fit_Chi2_NDF = r.TLatex(xmin+(xmax-xmin)*0.65, ymax*0.65, str(round(FitFunc.GetChisquare(),3)) + " / " + str(FitFunc.GetNDF()) + " = " + str(round(FitFunc.GetChisquare()/FitFunc.GetNDF(),3)))
    Fit_Chi2_NDF_Str = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.65, "#chi^{2} / NDF: ")
    Fit_Chi2_NDF.SetTextFont(42)
    Fit_Chi2_NDF.SetTextSize(0.023)
    Fit_Chi2_NDF.SetTextColor(r.kAzure+4)
    Fit_Chi2_NDF_Str.SetTextFont(62)
    Fit_Chi2_NDF_Str.SetTextSize(0.023)
    Fit_Chi2_NDF_Str.Draw()
    Fit_Chi2_NDF.Draw()

    # Prepare the text box to write the fit results
    BkgBox = r.TBox(xmin+(xmax-xmin)*0.53, ymax*0.72, xmin+(xmax-xmin)*0.92, ymax*0.9)
    BkgBox.Draw()

    PedestalStr = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.85, "Pedestal: ")
    PeakNameStr = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.8, "Gaus Peak: ")
    BkgStr = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.75, "Background: ")

    PedMean = r.TLatex(xmin+(xmax-xmin)*0.7, ymax*0.85, str(round(PedestalPar[1], 4)) + " #pm " + str(round(PedestalPar[2], 4)))
    PeakMean = r.TLatex(xmin+(xmax-xmin)*0.7, ymax*0.8, str(round(par_total[1], 4)) + " #pm " + str(round(par_total[2], 4)))
    BkgEquation = r.TLatex(xmin+(xmax-xmin)*0.7, ymax*0.75, str(round(par_total[3], 2)) + " e^{"+str(round(par_total[4], 5))+"x}"+str(round(par_total[5], 3)))

    StrList = [PedestalStr, PeakNameStr, BkgStr]
    NumList = [PedMean, PeakMean, BkgEquation]

    for s in StrList:
        s.SetTextFont(62)
        s.SetTextSize(0.023)
        s.Draw()

    for n in NumList:
        n.SetTextFont(42)
        n.SetTextSize(0.023)
        n.SetTextColor(r.kAzure+4)
        n.Draw()

    Gaus1.SetParameters(par_total[0],par_total[1],par_total[2])
    Gaus1.SetLineColor( r.kAzure+4 )
    Gaus1.SetLineWidth( -1504 )
    Gaus1.SetFillColorAlpha(r.kAzure+2, 0.5)
    Gaus1.SetFillStyle( 3001 )
    Gaus1.Draw('SAME')

    r.gPad.Modified()
    r.gPad.Update()
    c1.Draw()
    c1.SaveAs(hist_name+'_GausPeak.png')


def RunLandauOnBkgFit(hist, hist_name, peak, xmin, xmax, comment, PedestalPar, sigma):

    c1 = r.TCanvas("canv","",2600,2000)

    Landau1 = r.TF1("Landau1", 'landau', peak-sigma, peak+sigma*4)
    FitFunc = r.TF1( 'FitFunc', '[0]*TMath::Landau(x,[1],[2])+[3]*TMath::Exp(x*[4])+[5]', xmin, xmax )

    hist.Fit( Landau1, '0', '',  peak-100, peak+100)

    FitFunc.SetParameter(0, Landau1.GetParameters()[0])
    FitFunc.SetParameter(1, Landau1.GetParameters()[1])
    FitFunc.SetParameter(2, Landau1.GetParameters()[2])
    FitFunc.SetParameter(3, 150 )
    FitFunc.SetParameter(4, -5*10e-4)
    FitFunc.SetParameter(5, -2)

    FitFunc.SetLineColorAlpha( r.kPink-9, 0.3 )
    FitFunc.SetLineWidth( 6 )

    hist.SetLineWidth( 4 )
    hist.SetLineColor( r.kBlack )
    hist.SetMarkerStyle( 8 )
    hist.SetMarkerSize( 1.4 )

    hist.GetYaxis().SetTitle("NEvents")
    hist.GetXaxis().SetTitle("ADC")
    hist.GetXaxis().SetRangeUser(xmin,xmax)
    hist.SetTitle("Light yield measurement for " + comment)

    hist.Fit( FitFunc, 'R+' )
    FitFunc.Draw()
    hist.Draw("E0")

    ymax = hist.GetMaximum()

    par_total = FitFunc.GetParameters()
    # par_errors = FitFunc.GetParErrors()
    print('NDF: ', FitFunc.GetNDF())
    print('Chi2: ', FitFunc.GetChisquare())

    Fit_Chi2_NDF = r.TLatex(xmin+(xmax-xmin)*0.65, ymax*0.65, str(round(FitFunc.GetChisquare(),3)) + " / " + str(FitFunc.GetNDF()) + " = " + str(round(FitFunc.GetChisquare()/FitFunc.GetNDF(),3)))
    Fit_Chi2_NDF_Str = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.65, "#chi^{2} / NDF: ")
    Fit_Chi2_NDF.SetTextFont(42)
    Fit_Chi2_NDF.SetTextSize(0.023)
    Fit_Chi2_NDF.SetTextColor(r.kAzure+4)
    Fit_Chi2_NDF_Str.SetTextFont(62)
    Fit_Chi2_NDF_Str.SetTextSize(0.023)
    Fit_Chi2_NDF_Str.Draw()
    Fit_Chi2_NDF.Draw()

    # Prepare the text box to write the fit results
    BkgBox = r.TBox(xmin+(xmax-xmin)*0.53, ymax*0.72, xmin+(xmax-xmin)*0.92, ymax*0.9)
    BkgBox.Draw()

    PedestalStr = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.85, "Pedestal: ")
    PeakNameStr = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.8, "Landau Peak: ")
    BkgStr = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.75, "Background: ")

    PedMean = r.TLatex(xmin+(xmax-xmin)*0.7, ymax*0.85, str(round(PedestalPar[1], 4)) + " #pm " + str(round(PedestalPar[2], 4)))
    PeakMean = r.TLatex(xmin+(xmax-xmin)*0.7, ymax*0.8, str(round(par_total[1], 4)) + " #pm " + str(round(par_total[2], 4)))
    BkgEquation = r.TLatex(xmin+(xmax-xmin)*0.7, ymax*0.75, str(round(par_total[3], 2)) + " e^{"+str(round(par_total[4], 5))+"x}"+str(round(par_total[5], 3)))

    StrList = [PedestalStr, PeakNameStr, BkgStr]
    NumList = [PedMean, PeakMean, BkgEquation]

    for s in StrList:
        s.SetTextFont(62)
        s.SetTextSize(0.023)
        s.Draw()

    for n in NumList:
        n.SetTextFont(42)
        n.SetTextSize(0.023)
        n.SetTextColor(r.kAzure+4)
        n.Draw()

    Landau1.SetParameters(par_total[0],par_total[1],par_total[2])
    Landau1.SetLineColor( r.kAzure+4 )
    Landau1.SetLineWidth( -1504 )
    Landau1.SetFillColorAlpha(r.kAzure+2, 0.5)
    Landau1.SetFillStyle( 3001 )
    Landau1.Draw('SAME')

    r.gPad.Modified()
    r.gPad.Update()
    c1.Draw()
    c1.SaveAs(hist_name+'_LandauPeak.png')


def RunLangauOnBkgFit(hist, hist_name, peak, xmin, xmax, comment, PedestalPar, sigma):
    
    class langaus:
        def __call__(self, t, par):

            # width_landau = par[0]
            # MP = par[1]
            # Area = par[2]
            # widt_gauss = par[3]
            x = t[0]

            invsq2pi = 0.3989422804014 #(2 pi)^(-1/2)
            mpshift  = -0.22278298     #Landau maximum location

            np = 100.0  # number of convolution steps
            sc =   5.0  # convolution extends to +-sc Gaussian sigmas

            mpc = par[1] - mpshift * par[0]

            xlow = x - sc * par[3]
            xupp = x + sc * par[3]
            step = (xupp-xlow) / np
            
            summ = 0.0

            for i in range(1, int(np/2)):
                xx = xlow + (i-.5) * step
                fland = r.TMath.Landau(xx,mpc,par[0]) / par[0]
                summ += fland * r.TMath.Gaus(x,xx,par[3])

                xx = xupp - (i-.5) * step
                fland = r.TMath.Landau(xx,mpc,par[0]) / par[0]
                summ += fland * r.TMath.Gaus(x,xx,par[3])

            return (par[2] * step * summ * invsq2pi / par[3])

    class langausWbkg:
        def __call__(self, t, par):

            # width_landau = par[0]
            # MP = par[1]
            # Area = par[2]
            # widt_gauss = par[3]
            # exp_norm = par[4]
            # exp_const = par[5]
            # norm_const = par[6]
            x = t[0]

            invsq2pi = 0.3989422804014 #(2 pi)^(-1/2)
            mpshift  = -0.22278298     #Landau maximum location

            # np = 100.0  # number of convolution steps
            np = 100.0  # number of convolution steps
            sc =   5.0  # convolution extends to +-sc Gaussian sigmas

            mpc = par[1] - mpshift * par[0]

            xlow = x - sc * par[3]
            xupp = x + sc * par[3]
            step = (xupp-xlow) / np
            
            summ = 0.0
            for i in range(1, int(np/2)):
                xx = xlow + (i-.5) * step
                fland = r.TMath.Landau(xx,mpc,par[0]) / par[0]
                summ += fland * r.TMath.Gaus(x,xx,par[3])

                xx = xupp - (i-.5) * step
                fland = r.TMath.Landau(xx,mpc,par[0]) / par[0]
                summ += fland * r.TMath.Gaus(x,xx,par[3])

            return (par[2] * step * summ * invsq2pi / par[3]) + par[4]*r.TMath.Exp(x*par[5]) + par[6]
    
    c1 = r.TCanvas("canv","",2600,2000)

    langaus = langaus()
    langausWbkg = langausWbkg()
    Langau1 = r.TF1("langaus", langaus, peak-sigma, peak+sigma*4, 4)
    # FitFunc = r.TF1Convolution("langaus", "expo", xmin, xmax, True)
    FitFunc = r.TF1( 'FitFunc', langausWbkg, xmin, xmax, 7)

    Langau1.SetParameter(0, peak/20)
    Langau1.SetParameter(1, peak)
    Langau1.SetParameter(2, 5*peak)
    Langau1.SetParameter(3, peak/10)

    hist.Fit( Langau1, '0', '',  peak-100, peak+100)

    FitFunc.SetParameter(0, Langau1.GetParameters()[0])
    FitFunc.SetParameter(1, Langau1.GetParameters()[1])
    FitFunc.SetParameter(2, Langau1.GetParameters()[2])
    FitFunc.SetParameter(3, Langau1.GetParameters()[3])
    FitFunc.SetParameter(4, 150 )
    FitFunc.SetParameter(5, -5*10e-4)
    FitFunc.SetParameter(6, -2)

    FitFunc.SetLineColorAlpha( r.kPink-9, 0.3 )
    FitFunc.SetLineWidth( 6 )

    hist.SetLineWidth( 4 )
    hist.SetLineColor( r.kBlack )
    hist.SetMarkerStyle( 8 )
    hist.SetMarkerSize( 1.4 )

    hist.GetYaxis().SetTitle("NEvents")
    hist.GetXaxis().SetTitle("ADC")
    hist.GetXaxis().SetRangeUser(xmin,xmax)
    hist.SetTitle("Light yield measurement for " + comment)

    hist.Fit( FitFunc, 'R+' )
    FitFunc.Draw()
    hist.Draw("E0")

    ymax = hist.GetMaximum()

    par_total = FitFunc.GetParameters()
    langau_max = FitFunc.GetMaximumX(peak-500, peak+500)
    # par_errors = FitFunc.GetParErrors()
    print('NDF: ', FitFunc.GetNDF())
    print('Chi2: ', FitFunc.GetChisquare())

    Fit_Chi2_NDF = r.TLatex(xmin+(xmax-xmin)*0.65, ymax*0.6, str(round(FitFunc.GetChisquare(),3)) + " / " + str(FitFunc.GetNDF()) + " = " + str(round(FitFunc.GetChisquare()/FitFunc.GetNDF(),3)))
    Fit_Chi2_NDF_Str = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.6, "#chi^{2} / NDF: ")
    Fit_Chi2_NDF.SetTextFont(42)
    Fit_Chi2_NDF.SetTextSize(0.023)
    Fit_Chi2_NDF.SetTextColor(r.kAzure+4)
    Fit_Chi2_NDF_Str.SetTextFont(62)
    Fit_Chi2_NDF_Str.SetTextSize(0.023)
    Fit_Chi2_NDF_Str.Draw()
    Fit_Chi2_NDF.Draw()

    # Prepare the text box to write the fit results
    BkgBox = r.TBox(xmin+(xmax-xmin)*0.53, ymax*0.67, xmin+(xmax-xmin)*0.92, ymax*0.9)
    BkgBox.Draw()

    PedestalStr = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.85, "Pedestal: ")
    PeakNameStr1 = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.8, "Landau Peak: ")
    PeakNameStr2 = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.75, "Actual Peak: ")
    BkgStr = r.TLatex(xmin+(xmax-xmin)*0.55, ymax*0.7, "Background: ")

    PedMean = r.TLatex(xmin+(xmax-xmin)*0.7, ymax*0.85, str(round(PedestalPar[1], 4)) + " #pm " + str(round(PedestalPar[2], 4)))
    PeakMean1 = r.TLatex(xmin+(xmax-xmin)*0.7, ymax*0.8, str(round(par_total[1], 4)) + " #pm " + str(round(par_total[0], 4)))
    PeakMean2 = r.TLatex(xmin+(xmax-xmin)*0.7, ymax*0.75, str(round(langau_max, 4)) + " #pm " + str(round(math.sqrt(par_total[0]**2 + par_total[3]**2), 4)))
    BkgEquation = r.TLatex(xmin+(xmax-xmin)*0.7, ymax*0.7, str(round(par_total[4], 2)) + " e^{"+str(round(par_total[5], 5))+"x}"+str(round(par_total[6], 3)))

    StrList = [PedestalStr, PeakNameStr1, PeakNameStr2, BkgStr]
    NumList = [PedMean, PeakMean1, PeakMean2, BkgEquation]

    for s in StrList:
        s.SetTextFont(62)
        s.SetTextSize(0.023)
        s.Draw()

    for n in NumList:
        n.SetTextFont(42)
        n.SetTextSize(0.023)
        n.SetTextColor(r.kAzure+4)
        n.Draw()

    Langau1.SetParameters(par_total[0],par_total[1],par_total[2], par_total[3])
    Langau1.SetLineColor( r.kAzure+4 )
    Langau1.SetLineWidth( -1504 )
    Langau1.SetFillColorAlpha(r.kAzure+2, 0.5)
    Langau1.SetFillStyle( 3001 )
    Langau1.Draw('SAME')

    r.gPad.Modified()
    r.gPad.Update()
    c1.Draw()
    c1.SaveAs(hist_name+'_LangauPeak.png')


if __name__ == '__main__':
    sys.exit(main())
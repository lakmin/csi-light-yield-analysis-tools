import ROOT as r
import numpy as np
import argparse
import sys
from array import array
import math
# import ctypes

### Default ROOT Settings
r.TH1.AddDirectory(False)

def main():
    r.gROOT.SetBatch(True)
    r.gStyle.SetOptStat(0)

    args = GetParser()
    input_file = args.File
    hist_name = args.Hist
    hist_num = args.HistNum

    hist_list = []

    hist_file = r.TFile.Open(input_file)
    for i in range(1, 8):
        hist = hist_file.Get(hist_name+"_"+str(i))
        hist.Rebin(5)
        hist_list.append(hist)

    # Run fit and catch the fit par's for the peaks
    FitParList1, ParErrorList1, FitParList2, ParErrorList2 =  RunOnePEfit(hist_list, hist_name)

    # Draw a linear graph with the peak ADC's. 
    # The last number represents the number of histograms to plot for.
    DrawGainGraph(FitParList1, ParErrorList1, FitParList2, ParErrorList2, hist_name, hist_num)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def GetParser():
    parser = argparse.ArgumentParser(
        description="This script is used to analyze the ADC output from 'Notice' fADC."
    )

    parser.add_argument(
        "-f",
        "--File",
        type=str,
        required=True,
        help="Path of the input root file"
    )

    parser.add_argument(
        "-n",
        "--Hist",
        type=str,
        required=True,
        help="Name of the histogram to fit"
    )

    parser.add_argument(
        "-i",
        "--HistNum",
        type=int,
        required=True,
        help="Number of histograms to plot iteratively for."
    )

    args = parser.parse_args()
    return args

def RunOnePEfit(hist_list, hist_name):

    c1 = r.TCanvas("canv","",2600,2000)
    PeakList = [-2.5, 87, 68, 59, 47, 33, 29, 26]
    PeakNameList = []
    PeakName = ["-2200 V", "-2150 V", "-2100 V", "-2050 V", "-1970 V", "-1940 V", "-1920 V"]

    Gaus0 = r.TF1("Gaus0", 'gaus', PeakList[0]-50, PeakList[0]+50)

    Gaus11 = r.TF1("Gaus1", 'gaus', PeakList[1]-200, PeakList[1]+200)
    Gaus12 = r.TF1("Gaus2", 'gaus', PeakList[2]-200, PeakList[2]+200)
    Gaus13 = r.TF1("Gaus3", 'gaus', PeakList[3]-200, PeakList[3]+200)
    Gaus14 = r.TF1("Gaus4", 'gaus', PeakList[4]-200, PeakList[4]+200)
    Gaus15 = r.TF1("Gaus5", 'gaus', PeakList[5]-200, PeakList[5]+200)
    Gaus16 = r.TF1("Gaus6", 'gaus', PeakList[6]-200, PeakList[6]+200)
    Gaus17 = r.TF1("Gaus7", 'gaus', PeakList[7]-200, PeakList[7]+200)

    Gaus21 = r.TF1("Gaus1", 'gaus', PeakList[1]*2-200, PeakList[1]*2+200)
    Gaus22 = r.TF1("Gaus2", 'gaus', PeakList[2]*2-200, PeakList[2]*2+200)
    Gaus23 = r.TF1("Gaus3", 'gaus', PeakList[3]*2-200, PeakList[3]*2+200)
    Gaus24 = r.TF1("Gaus4", 'gaus', PeakList[4]*2-200, PeakList[4]*2+200)
    Gaus25 = r.TF1("Gaus5", 'gaus', PeakList[5]*2-200, PeakList[5]*2+200)
    Gaus26 = r.TF1("Gaus6", 'gaus', PeakList[6]*2-200, PeakList[6]*2+200)
    Gaus27 = r.TF1("Gaus7", 'gaus', PeakList[7]*2-200, PeakList[7]*2+200)

    FitFunc1 = r.TF1( 'FitFunc1', 'gaus(0)+gaus(3)+gaus(6)', -50, 350 )

    GausList1 = [Gaus11, Gaus12, Gaus13, Gaus14, Gaus15, Gaus16, Gaus17]
    GausList2 = [Gaus21, Gaus22, Gaus23, Gaus24, Gaus25, Gaus26, Gaus27]
    par = array( 'd', 3*3*[0.] )

    # Save parameters for the linear fit
    ParList1PE = []
    ParErrorList1PE = []
    ParList2PE = []
    ParErrorList2PE = []

    for i in range(7):
        print(bcolors.OKCYAN+"Running for histogram "+str(i)+bcolors.ENDC)
        hist_list[i].Fit( Gaus0, '0', '',  PeakList[0]-5,PeakList[0]+5)
        hist_list[i].Fit( GausList1[i], '0+', '',  PeakList[i+1]-5,PeakList[i+1]+5)
        hist_list[i].Fit( GausList2[i], '0+', '',  PeakList[i+1]*2-5,PeakList[i+1]*2+5)

        par[0] = Gaus0.GetParameters()[0]
        par[1] = Gaus0.GetParameters()[1]
        par[2] = Gaus0.GetParameters()[2]
        par[3] = GausList1[i].GetParameters()[0]
        par[4] = GausList1[i].GetParameters()[1]
        par[5] = GausList1[i].GetParameters()[2]
        par[6] = GausList2[i].GetParameters()[0]
        par[7] = GausList2[i].GetParameters()[1]
        par[8] = GausList2[i].GetParameters()[2]

        FitFunc1.SetParameters( par )

        #######################################################

        FitFunc1.SetLineColorAlpha( r.kPink-9, 0.3 )
        FitFunc1.SetLineWidth( 6 )

        ymax = hist_list[i].GetMaximum()
        xmax = 350

        hist_list[i].SetLineWidth( 4 )
        hist_list[i].SetLineColor( r.kBlack )
        hist_list[i].SetMarkerStyle( 8 )
        hist_list[i].SetMarkerSize( 1.4 )

        hist_list[i].GetYaxis().SetTitle("NEvents")
        hist_list[i].GetXaxis().SetTitle("ADC")
        hist_list[i].SetTitle("PMT Gain Measurement using 1 p.e at "+PeakName[i])

        hist_list[i].Fit( FitFunc1, 'R+' )
        FitFunc1.Draw()
        hist_list[i].GetXaxis().SetRangeUser(-50,xmax)
        hist_list[i].Draw("E0")

        par_total = FitFunc1.GetParameters()
        par_errors = FitFunc1.GetParErrors()

        ParList1PE.append(par_total[4]-par_total[1])
        ParErrorList1PE.append(par_errors[4])
        ParList2PE.append(par_total[7]-par_total[1])
        ParErrorList2PE.append(par_errors[7])

        Fit_Chi2_NDF = r.TLatex(xmax*0.64, ymax*0.72, str(round(FitFunc1.GetChisquare(),3)) + " / " + str(FitFunc1.GetNDF()) + " = " + str(round(FitFunc1.GetChisquare()/FitFunc1.GetNDF(),3)))
        Fit_Chi2_NDF_Str = r.TLatex(xmax*0.45, ymax*0.72, "#chi^{2} / NDF: ")
        Fit_Chi2_NDF.SetTextFont(42)
        Fit_Chi2_NDF.SetTextSize(0.026)
        Fit_Chi2_NDF.SetTextColor(r.kAzure+4)
        Fit_Chi2_NDF_Str.SetTextFont(62)
        Fit_Chi2_NDF_Str.SetTextSize(0.026)
        Fit_Chi2_NDF_Str.Draw()
        Fit_Chi2_NDF.Draw()

        # Prepare the text box to write the fit results
        BkgBox = r.TBox(xmax*0.42, ymax*0.77, xmax*0.92, ymax*0.95)
        BkgBox.Draw()

        PedNameStr  = r.TLatex(xmax*0.45, ymax*(0.9),  " Pedestal: ")
        PeakNameStr1 = r.TLatex(xmax*0.45, ymax*(0.85), " 1 p.e Peak: ")
        PeakNameStr2 = r.TLatex(xmax*0.45, ymax*(0.8),  " 2 p.e Peak: ")

        PedMean  = r.TLatex(xmax*0.64, ymax*(0.9),  str(round(par_total[1], 4)) + " #pm " + str(round(par_total[2], 4)))
        PeakMean1 = r.TLatex(xmax*0.64, ymax*(0.85), str(round(par_total[4], 4)) + " #pm " + str(round(par_total[5], 4)))
        PeakMean2 = r.TLatex(xmax*0.64, ymax*(0.8),  str(round(par_total[7], 4)) + " #pm " + str(round(par_total[8], 4)))

        PedNameStr.SetTextFont(62)
        PeakNameStr1.SetTextFont(62)
        PeakNameStr2.SetTextFont(62)
        PedNameStr.SetTextSize(0.026)
        PeakNameStr1.SetTextSize(0.026)
        PeakNameStr2.SetTextSize(0.026)

        PedMean.SetTextFont(42)
        PeakMean1.SetTextFont(42)
        PeakMean2.SetTextFont(42)
        PedMean.SetTextSize(0.026)
        PedMean.SetTextColor(r.kAzure+4)
        PeakMean1.SetTextSize(0.026)
        PeakMean1.SetTextColor(r.kAzure+4)
        PeakMean2.SetTextSize(0.026)
        PeakMean2.SetTextColor(r.kAzure+4)

        Gaus0.SetParameters(par_total[0],par_total[1],par_total[2])
        GausList1[i].SetParameters(par_total[3],par_total[4],par_total[5])
        GausList2[i].SetParameters(par_total[6],par_total[7],par_total[8])

        Gaus0.SetLineColor( r.kAzure+4 )
        GausList1[i].SetLineColor( r.kAzure+4 )
        GausList2[i].SetLineColor( r.kAzure+4 )

        Gaus0.SetLineWidth( -1504 )
        GausList1[i].SetLineWidth( -1504 )
        GausList2[i].SetLineWidth( -1504 )

        Gaus0.SetFillColorAlpha(r.kAzure+2, 0.5)
        GausList1[i].SetFillColorAlpha(r.kAzure+5, 0.5)
        GausList2[i].SetFillColorAlpha(r.kAzure+5, 0.5)

        Gaus0.SetFillStyle( 3001 )
        GausList1[i].SetFillStyle( 3001 )
        GausList2[i].SetFillStyle( 3001 )

        Gaus0.Draw('SAME')
        GausList1[i].Draw('SAME')
        GausList2[i].Draw('SAME')

        PedNameStr.Draw()
        PeakNameStr1.Draw()
        PeakNameStr2.Draw()
        PedMean.Draw()
        PeakMean1.Draw()
        PeakMean2.Draw()

        r.gPad.Modified()
        r.gPad.Update()
        c1.Draw()
        c1.SaveAs(hist_name+'_'+str(i)+'.png')

    GausList1.reverse()
    GausList2.reverse()
    # Create plot with the gaus dists.
    c2 = r.TCanvas("canv1","",2600,2000)

    ColorList = [r.kAzure+7, r.kTeal+5, r.kSpring-5, r.kOrange+1, r.kPink-5, r.kViolet+5, r.kGray+1]

    for j, Gaus1 in enumerate(GausList1):
        Gaus1.SetFillColorAlpha(ColorList[j], 0.5)
        Gaus1.SetLineColor( r.kGray+2 )

        if j == 1:
            Gaus1.SetRange(-100,0,300,3000)
            Gaus1.GetYaxis().SetTitle("NEvents")
            Gaus1.GetXaxis().SetTitle("ADC")
            Gaus1.SetTitle("PMT Gain Measurement - 1 p.e at different gains")
            Gaus1.Draw()
        else:
            Gaus1.Draw('SAME')

    c2.SaveAs(hist_name+'_all1.png')

    c3 = r.TCanvas("canv2","",2600,2000)

    for j, Gaus2 in enumerate(GausList2):
        Gaus2.SetFillColorAlpha(ColorList[j], 0.5)
        Gaus2.SetLineColor( r.kGray+2 )

        if j == 1:
            Gaus2.SetRange(-100,0,350,3000)
            Gaus2.GetYaxis().SetTitle("NEvents")
            Gaus2.GetXaxis().SetTitle("ADC")
            Gaus2.SetTitle("PMT Gain Measurement - 2 p.e at different gains")
            Gaus2.Draw()
        else:
            Gaus2.Draw('SAME')

    c3.SaveAs(hist_name+'_all2.png')

    return ParList1PE, ParErrorList1PE, ParList2PE, ParErrorList2PE


def DrawGainGraph(parlist1PE, par_errorlist1PE, parlist2PE, par_errorlist2PE, hist_name, ParLength):
    c1 = r.TCanvas("canv","",2600,2000)

    HVList = array('d', [2200, 2150, 2100, 2050, 1970, 1940, 1920])
    ADC_vals_1PE = array('d', parlist1PE)
    ADC_err_1PE = array('d', par_errorlist1PE)
    ADC_vals_2PE = array('d', parlist2PE)
    ADC_err_2PE = array('d', par_errorlist2PE)
    
    LinGraph1 = r.TGraphErrors(ParLength, HVList, ADC_vals_1PE, r.nullptr, ADC_err_1PE)
    # LinGraph1.SetRange(1900,10,2250,180)
    LinGraph1.SetMinimum(10)
    LinGraph1.SetMaximum(180)
    LinGraph1.SetMarkerColor(r.kPink+4)
    LinGraph1.SetMarkerStyle(8)
    LinGraph1.SetMarkerSize(3)
    LinGraph1.SetLineColor(r.kPink+9)
    LinGraph1.SetLineWidth(7)
    LinGraph1.Draw("AP")

    LinGraph2 = r.TGraphErrors(ParLength, HVList, ADC_vals_2PE, r.nullptr, ADC_err_2PE)
    LinGraph2.SetMarkerColor(r.kAzure+4)
    LinGraph2.SetMarkerStyle(8)
    LinGraph2.SetMarkerSize(3)
    LinGraph2.SetLineColor(r.kTeal)
    LinGraph2.SetLineWidth(7)
    LinGraph2.Draw("PSAME")

    # Define a function for PMT gain
    f_pg1 = r.TF1("f_pg1", "[0]*TMath::Power(x,[1])+[2]", 1900, 2250)
    f_pg2 = r.TF1("f_pg2", "[0]*TMath::Power(x,[1])+[2]", 1900, 2250)
    # f_pg.SetParLimits(0,0.02,0.05)
    f_pg1.SetParLimits(1,4,10)
    f_pg2.SetParLimits(1,4,10)

    # The linear fit
    LinGraph1.Fit(f_pg1, "R0")
    LinGraph2.Fit(f_pg2, "R0")

    f_pg1.SetLineColor(r.kGray+1)
    f_pg1.SetLineStyle(10)
    f_pg1.SetLineWidth(3)
    f_pg1.Draw("SAME")

    f_pg2.SetLineColor(r.kGray+1)
    f_pg2.SetLineStyle(10)
    f_pg2.SetLineWidth(3)
    f_pg2.Draw("SAME")

    par_total1 = f_pg1.GetParameters()
    par_total2 = f_pg2.GetParameters()

    ############ Adding cosmetics to the graph ############
    LinGraph1.GetYaxis().SetTitle("ADC")
    LinGraph1.GetXaxis().SetTitle("HV [-V]")
    LinGraph1.SetTitle("PMT Gain Measurement - Gain Curve for 1 p.e and 2 p.e")

    # A hack to get the significant digits
    SigDigits1 = round(math.log10(par_total1[0]),0)
    SigDigits2 = round(math.log10(par_total2[0]),0)

    PolFunction1 = r.TLatex(2050, 30, "y = " + str(round(par_total1[0]/pow(10,SigDigits1),3)) + "#times 10^{" + str(SigDigits1) + "} x^{" + str(round(par_total1[1], 3)) + "} " + str(round(par_total1[2], 3)))
    PolFunction2 = r.TLatex(1930, 130, "y = " + str(round(par_total2[0]/pow(10,SigDigits2),3)) + "#times 10^{" + str(SigDigits2) + "} x^{" + str(round(par_total2[1], 3)) + "} " + str(round(par_total2[2], 3)))

    PolFunction1.SetTextFont(42)
    PolFunction1.SetTextSize(0.04)
    PolFunction1.SetTextColor(r.kAzure+4)
    PolFunction1.Draw()

    PolFunction2.SetTextFont(42)
    PolFunction2.SetTextSize(0.04)
    PolFunction2.SetTextColor(r.kAzure+4)
    PolFunction2.Draw()
    #######################################################

    c1.Draw()
    c1.SaveAs(hist_name+'_linear.png')

if __name__ == '__main__':
    sys.exit(main())
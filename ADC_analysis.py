import ROOT as r
import argparse
import sys, os
import numpy as np
from array import array

# import ctypes

### Default ROOT Settings
r.TH1.AddDirectory(False)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def GetParser():
    parser = argparse.ArgumentParser(
        description="This script is used to analyze the ADC output from 'Notice' fADC."
    )

    parser.add_argument(
        "-f",
        "--File",
        type=str,
        required=True,
        help="Path of the input root file"
    )

    parser.add_argument(
        "-o",
        "--OutFile",
        type=str,
        required=True,
        help="Path of the output root file"
    )

    parser.add_argument(
        "-c",
        "--Comment",
        type=str,
        required=True,
        help="Comment to the output file"
    )

    parser.add_argument(
        "-s",
        "--Sigma",
        type=int,
        required=True,
        help="The sigma cut to remove pedestal"
    )

    parser.add_argument(
        "-x",
        "--XMax",
        type=int,
        required=True,
        help="The maximum for the x-range"
    )

    parser.add_argument(
        "--SplitADC",
        action="store_true",
        help="Turn this flag if you wish to split the corrected ADC hist into 2, from 0-100, and 100~"
    )

    args = parser.parse_args()
    return args

def main():
    args = GetParser()
    ntuple_file = args.File
    output_file = args.OutFile
    comment = args.Comment
    sigma_cut = args.Sigma
    xmax = args.XMax

    ####### Define histogram ########
    canv = r.TCanvas("canv_ADC","",1500,1200)
    r.SetOwnership(canv, False)
    # w = np.ones(1000)
    if xmax > 2000:
        xmin = 0
        nbin = int(xmax / 5)
    else:
        xmin = -50
        nbin = xmax-xmin
        
    ADC_hist = r.TH1F('ADC_Hist', 'ADC_Hist', nbin, xmin, xmax)

    #### output_file ####
    if os.path.exists(output_file):
        outFile = r.TFile.Open(output_file, "UPDATE")
    else:
        outFile = r.TFile.Open(output_file, "RECREATE")


    #### read input file
    fadc_file = r.TFile.Open(ntuple_file)
    fadc_tree = fadc_file.Get("nfadc400")

    Reco_ADC = []
    #Decide which part to cut in the FADC
    baseline_cut_end = 100
    SR_cut_start = 150
    SR_cut_end = 250

    for i, entry in enumerate(fadc_tree):
        if i % 1000 == 0:
            print("Done with " + bcolors.WARNING + str(i) + " events" + bcolors.ENDC)

        fadc_selected = []

        fadc = getattr(entry, 'fadc/FADC')
        tag = getattr(entry, 'fadc/tag')
        trigger = getattr(entry, 'fadc/trigger')

        fadc.reshape((2048,))
        tag.reshape((6,))
        trigger.reshape((2,))
        
        # Channel 1~4 entries are shown in the list from 
        # 0~511, 512~1023, 1024~1535, 1536~2048 respectively.
        baseline_list = list(fadc)[0:baseline_cut_end]
        SR_list = list(fadc)[SR_cut_start:SR_cut_end]
        # tag_list = list(tag)
        # trigger_list = list(trigger)

        #Calculate the value for baseline
        baseline_avg = np.mean(baseline_list)
        baseline_std = np.std(baseline_list)

        for i in SR_list:
            new_ADC_val = -1*(i - baseline_avg)
            # Minus values should also be left in the list
            # so the sum will reduce the minus from the pedestal
            fadc_selected.append(round(new_ADC_val,3))

        temp_val = round(sum(fadc_selected),4)
        #Avoid filling zeros
        #if temp_val>0:
        ADC_hist.Fill(temp_val)
        # Reco_ADC.append(round(sum(fadc_selected),4))

    outFile.WriteObject(ADC_hist, "ADC_hist_"+comment)

    # print(Reco_ADC)
    # ADC_hist.Draw()
    # canv.SaveAs(comment+".png")

if __name__ == '__main__':
    sys.exit(main())

